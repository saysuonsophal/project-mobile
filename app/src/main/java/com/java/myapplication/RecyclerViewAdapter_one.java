package com.java.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter_one extends RecyclerView.Adapter<RecyclerViewAdapter_one.MyViewHolder> {

    //Contect is abstract class
    private Context mContext;
    //this mData is list that manage all item recyclecard
    private List<food_one> mData;

    public RecyclerViewAdapter_one(Context mContext, List<food_one> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        //this is calling layout recyclecard_one to using
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.recyclecard_one,viewGroup,false);
        return new MyViewHolder(view);
    }

    //it taking img and title
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.tv_food_title.setText(mData.get(i).getTitle());
        myViewHolder.tv_food_img.setImageResource(mData.get(i).getThumbnail());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public  static  class  MyViewHolder extends RecyclerView.ViewHolder{

        //this get value to taking image and title
        TextView tv_food_title;
        ImageView tv_food_img;

        public MyViewHolder(View itemView){

            super(itemView);

            tv_food_img =(ImageView) itemView.findViewById(R.id.one_img_id);
            tv_food_title=(TextView) itemView.findViewById(R.id.one_title_id);
        }
    }
}
