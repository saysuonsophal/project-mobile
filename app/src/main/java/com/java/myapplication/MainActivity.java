package com.java.myapplication;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.java.myapplication.Fragment.EightFragment;
import com.java.myapplication.Fragment.FiveFragment;
import com.java.myapplication.Fragment.FourFragment;
import com.java.myapplication.Fragment.OneFragment;
import com.java.myapplication.Fragment.SevenFragment;
import com.java.myapplication.Fragment.SixFragment;
import com.java.myapplication.Fragment.ThreeFragment;
import com.java.myapplication.Fragment.TwoFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    TextView tvInfo;
    TabLayout tabLayout;
    ViewPager viewPager;
    RecyclerView dataList_one;
    List<food_one> lstfood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//   *   Header1 (Making NavigationView)

        //this is calling toolbar to using
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //this is text showing action
        tvInfo =(TextView) findViewById(R.id.tv_Info);
        //this is homelayout
        drawerLayout=(DrawerLayout) findViewById(R.id.drawer_layout);

        //this is layout Navigationbar that view
        NavigationView navigationView=(NavigationView) findViewById(R.id.drawer);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle drawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,
               R.string.drawer_open,R.string.drawer_close);
        //this is action of menubar
        drawerLayout.addDrawerListener(drawerToggle);
        //this use to chagne menubar when use click
        drawerToggle.syncState();

//   *   Header2 (Making TabLayout)

        //menu scroll
        tabLayout =(TabLayout) findViewById(R.id.tablayout_id);
        //this view is body
        viewPager=(ViewPager) findViewById(R.id.viewPager_id);
        //viewpager adding one by one fragment
        ViewPageAdapter viewPageAdapter= new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragment(new OneFragment()," ITEM ONE ");
        viewPageAdapter.addFragment(new TwoFragment()," ITEM TWO ");
        viewPageAdapter.addFragment(new ThreeFragment()," ITEM THREE ");
        viewPageAdapter.addFragment(new FourFragment()," ITEM FOUR ");
        viewPageAdapter.addFragment(new FiveFragment()," ITEM FIVE ");
        viewPageAdapter.addFragment(new SixFragment()," ITEM SIX ");
        viewPageAdapter.addFragment(new SevenFragment()," ITEM SEVEN ");
        viewPageAdapter.addFragment(new EightFragment()," ITEM EIGHT ");
        //taking to put in vic
        viewPager.setAdapter(viewPageAdapter);

       tabLayout.setupWithViewPager(viewPager);

//    *   Recycleview gridlayout of body

        //this is adding one by one item fodd
//        dataList_one =(RecyclerView) findViewById(R.id.dataList_one);
        lstfood = new ArrayList<>();
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food1));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food2));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food3));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food4));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food5));
        lstfood.add(new food_one("No Name","Categorie food","Description food",R.drawable.food6));


        RecyclerView myrv=(RecyclerView) findViewById(R.id.recycleview_one);
        RecyclerViewAdapter_one myAdapter = new RecyclerViewAdapter_one(this,lstfood);
        myrv.setLayoutManager(new GridLayoutManager(this,2));



    }
    //this is action of Navigation bar
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        String menuitemName= (String) menuItem.getTitle();
        tvInfo.setText(menuitemName);

        closeDrawer();

        switch (menuItem.getItemId()){
            case R.id.item_a:
                break;
            case R.id.item_b:
                break;
        }
        return true;
    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }
    private void openDrawer(){
        drawerLayout.openDrawer(GravityCompat.START);
    }
    //this method use to show hompageback when NavigationView closed
    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            closeDrawer();
        }
        super.onBackPressed();
    }
}
